package ch.fhnw.algd2.arraycollections;

import java.util.Arrays;

public class UnsortedBag<E> extends AbstractArrayCollection<E> {
	public static final int DEFAULT_CAPACITY = 100;
	private Object[] data;
	private int size = 0;

	public UnsortedBag() {
		this(DEFAULT_CAPACITY);
	}

	public UnsortedBag(int capacity) {
		data = new Object[capacity];
	}

	@Override
	public boolean add(E e) {
		if (e == null)
			throw new NullPointerException();

		if (size == data.length)
			throw new IllegalStateException();

		data[size++] = e;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null)
			throw new NullPointerException();

		int i = 0;
		while (i < size && !data[i].equals(o)) {
			i++;
		}

		if (i == size)
			return false;

		data[i] = data[size - 1];
		
		data[size - 1] = null;

		size--;

		return true;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null)
			throw new NullPointerException();

		int i = 0;
		while (i < size && !data[i].equals(o)) {
			i++;
		}

		return i < size;
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(data, size());
	}

	@Override
	public int size() {
		return size;
	}

	public static void main(String[] args) {
		UnsortedBag<Integer> bag = new UnsortedBag<Integer>();
		bag.add(2);
		bag.add(1);
		System.out.println(Arrays.toString(bag.toArray()));
	}
}
