package ch.fhnw.algd2.arraycollections;

import java.util.Arrays;

public class SortedBag<E extends Comparable<? super E>> extends
		AbstractArrayCollection<E> {
	public static final int DEFAULT_CAPACITY = 100;
	private Object[] data;
	private int size = 0;

	public SortedBag() {
		this(DEFAULT_CAPACITY);
	}

	public SortedBag(int capacity) {
		data = new Object[capacity];
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E e) {
		if (e == null)
			throw new NullPointerException();
		
		if (size == data.length)
			throw new IllegalStateException();

		int i = size++;

		while (i > 0 && e.compareTo((E) data[i - 1]) < 0) {
			data[i] = data[i - 1];
			i--;
		}
		
		data[i] = e;

		return true;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null)
			throw new NullPointerException();

		int i = 0;
		while (i < size && !data[i].equals(o)) {
			i++;
		}

		if (i == size)
			return false;

		while (i < size - 1) {
			data[i] = data[i + 1];
			i++;
		}

		data[size - 1] = null;

		size--;

		return true;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null)
			throw new NullPointerException();

		return contains(o, 0, size - 1);
	}

	@SuppressWarnings("unchecked")
	private boolean contains(Object o, int l, int r) {
		if (l > r)
			return false;

		int m = (l + r) / 2;

		if (((E) data[m]).compareTo((E) o) < 0)
			return contains(o, m + 1, r);

		if (((E) data[m]).compareTo((E) o) > 0)
			return contains(o, l, m - 1);

		return true;
	}
	
	@Override
	public Object[] toArray() {
		return Arrays.copyOf(data, size());
	}

	@Override
	public int size() {
		return size;
	}

	public static void main(String[] args) {
		SortedBag<Integer> bag = new SortedBag<Integer>();
		bag.add(2);
		bag.add(1);
		System.out.println(Arrays.toString(bag.toArray()));
	}
}
