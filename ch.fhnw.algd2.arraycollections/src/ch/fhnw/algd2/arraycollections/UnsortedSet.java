package ch.fhnw.algd2.arraycollections;

import java.util.Arrays;
import java.util.Set;

public class UnsortedSet<E> extends AbstractArrayCollection<E> implements Set<E> {
	public static final int DEFAULT_CAPACITY = 100;
	private Object[] data;
	private int size = 0;

	public UnsortedSet() {
		this(DEFAULT_CAPACITY);
	}

	public UnsortedSet(int capacity) {
		data = new Object[capacity];
	}

	@Override
	public boolean add(E e) {
		if (e == null)
			throw new NullPointerException();

		if (!contains(e)) {
			if (size == data.length)
				throw new IllegalStateException();

			data[size++] = e;
			return true;
		}

		return false;
	}

	@Override
	public boolean remove(Object o) {
		if (o == null)
			throw new NullPointerException();

		int i = 0;
		while (i < size && !data[i].equals(o)) {
			i++;
		}

		if (i == size)
			return false;

		data[i] = data[size - 1];
		
		data[size - 1] = null;

		size--;

		return true;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null)
			throw new NullPointerException();

		int i = 0;
		while (i < size && !data[i].equals(o)) {
			i++;
		}

		return i < size;
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(data, size());
	}

	@Override
	public int size() {
		return size;
	}

	public static void main(String[] args) {
		UnsortedSet<Integer> bag = new UnsortedSet<Integer>();
		bag.add(2);
		bag.add(2);
		bag.add(1);
		System.out.println(Arrays.toString(bag.toArray()));
	}
}
