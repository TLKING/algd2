package ch.fhnw.algd2.collections.list.linkedlist;

import java.util.Arrays;

import ch.fhnw.algd2.collections.list.MyAbstractList;

public class MyLinkedList<E> extends MyAbstractList<E> {
	private int size = 0;
	private Node<E> first;

	@Override
	public boolean add(E e) {
		// if you want to ignore the spec
		// first = new Node<E>(e, first);

		if (first == null) { // handle first entry differently
			first = new Node<E>(e);
		} else {
			Node<E> node = first;

			while (node.next != null) {
				node = node.next;
			}

			node.next = new Node<E>(e);
		}
		size++;

		return true;
	}

	@Override
	public boolean contains(Object o) {
		if (first == null)
			return false;

		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			node = node.next;
		}
		return o == node.elem || (o != null && o.equals(node.elem));

	}

	@Override
	public boolean remove(Object o) {
		if (first == null || !contains(o))
			return false;

		Node<E> prev = null;
		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			prev = node;
			node = node.next;
		}

		if (prev == null) {
			first = node.next;
		} else {
			prev.next = node.next;
		}

		size--;

		return true;
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		while (index > 0) {
			node = node.next;
			index--;
		}

		return node.elem;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index > size)
			throw new IndexOutOfBoundsException();

		if (index == 0) {
			first = new Node<E>(element, first);
		} else {
			Node<E> prev = null;
			Node<E> node = first;

			while (index > 0) {
				prev = node;
				node = node.next;
				index--;
			}

			prev.next = new Node<E>(element, node);
		}

		size++;
	}

	@Override
	public E remove(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		if (index == 0) {
			first = first.next;
		} else {
			Node<E> prev = null;

			while (index > 0) {
				prev = node;
				node = node.next;
				index--;
			}

			if (prev == null) {
				first = node.next;
			} else {
				prev.next = node.next;
			}
		}

		size--;

		return node.elem;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		int index = 0;
		Node<E> current = first;
		while (current != null) {
			array[index++] = current.elem;
			current = current.next;
		}
		return array;
	}

	@Override
	public int size() {
		return size;
	}

	private static class Node<E> {
		private final E elem;
		private Node<E> next;

		private Node(E elem) {
			this.elem = elem;
		}

		private Node(E elem, Node<E> next) {
			this.elem = elem;
			this.next = next;
		}
	}

	public static void main(String[] args) {
		MyLinkedList<Integer> list = new MyLinkedList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		System.out.println(Arrays.toString(list.toArray()));
	}
}
