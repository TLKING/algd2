package ch.fhnw.algd2.heaptest;

/* Heap als Implementierung einer Priority Queue */
class Heap<K> implements PriorityQueue<K> {
	private HeapNode<K>[] heap; // Array to store the heap elements
	private int size; // position of last insertion into the heap

	/**
	 * Construct the binary heap.
	 * 
	 * @param size
	 *            how many items the heap can store
	 */
	@SuppressWarnings("unchecked")
	Heap(int capacity) {
		heap = new HeapNode[capacity];
	}

	/**
	 * Returns the number of elements in this priority queue.
	 * 
	 * @return the number of elements in this priority queue.
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Check whether the heap is empty.
	 * 
	 * @return true if there are no items in the heap.
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Check whether the heap is full.
	 * 
	 * @return true if no further elements can be inserted into the heap.
	 */
	@Override
	public boolean isFull() {
		return size == heap.length;
	}

	/**
	 * Make the heap (logically) empty.
	 */
	@Override
	public void clear() {
		for (int i = 0; i < size; i++)
			heap[i] = null;
	}

	/**
	 * Add to the priority queue, maintaining heap order. Duplicates and null
	 * values are allowed. Small values of the argument priority means high
	 * priority, Large values means low priority.
	 * 
	 * @param element
	 *            the item to insert
	 * @param priority
	 *            the priority to be assigned to the item element
	 * @exception QueueFullException
	 *                if the heap is full
	 */
	@Override
	public void add(K element, long priority) throws QueueFullException {
		if (isFull())
			throw new QueueFullException();
		heap[size] = new HeapNode<K>(element, priority);
		siftUp(size);
		size++;
	}

	/**
	 * Removes and returns the item with highest priority (smallest priority
	 * value) from the queue, maintaining heap order.
	 * 
	 * @return the item with highest priority (smallest priority value)
	 * @throws QueueEmptyException
	 *             if the queue is empty
	 */
	@Override
	public K removeMin() throws QueueEmptyException {
		if (isEmpty())
			throw new QueueEmptyException();
		K elem = heap[0].element;
		size--;
		swap(0, size);
		heap[size] = null;
		siftDown(0);
		return elem;
	}

	/**
	 * Internal method to let an element sift down in the heap.
	 * 
	 * @param start
	 *          the index at which the percolate begins
	 */
	private void siftDown(int start) {
		int parent = start;
		int childL = 2 * parent + 1;
		int childR = 2 * parent + 2;
		
		if (childR < size) {
			if (heap[childL].priority <= heap[childR].priority) {
				swap(parent, childL);
				siftDown(childL);
			} else {
				swap(parent, childR);
				siftDown(childR);
			}
		} else {
			// check if there is a left child and it is smaller than the parent
			if (childL < size && heap[parent].priority > heap[childL].priority)
				swap(parent, childL);
		}
	}

	/**
	 * Internal method to let an element sift up in the heap.
	 * 
	 * @param start
	 *            the index at which the percolate begins
	 */
	private void siftUp(int start) {
		int parent = (start - 1) / 2;

		if (heap[start].priority < heap[parent].priority) {
			swap(parent, start);
			siftUp(parent);
		}
	}

	private void swap(int a, int b) {
		HeapNode<K> tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}

	/**
	 * Erzeugt ein neues long[] Array und kopiert die Werte der Priorit�ten aus
	 * dem Heaparray dort hinein. Die Gr�sse des zur�ckgegebenen Arrays soll der
	 * Anzahl Elemente in der Queue entsprechen (= size()). An der Position 0
	 * soll die kleinste Priorit�t (= Priorit�t des Wurzelelementes) stehen.
	 * 
	 * @return Array mit allen Priorit�ten
	 */
	@Override
	public long[] toLongArray() {
		long[] res = new long[size];
		for (int i = 0; i < size; i++)
			res[i] = heap[i].priority;
		return res;
	}

	private static class HeapNode<K> {
		private final K element;
		private final long priority;

		HeapNode(K element, long priority) {
			this.element = element;
			this.priority = priority;
		}
	}
}
