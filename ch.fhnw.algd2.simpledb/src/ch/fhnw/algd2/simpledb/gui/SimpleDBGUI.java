/*
 * Created on 08.05.2015
 */
package ch.fhnw.algd2.simpledb.gui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.util.StringConverter;
import ch.fhnw.algd2.simpledb.database.SimpleDB;

/**
 * @author Wolfgang Weck
 */
public class SimpleDBGUI {
	private final SimpleDB database = new SimpleDB();
	private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	private StringProperty name;
	private ObjectProperty<Date> date = new SimpleObjectProperty<>();
	private BooleanProperty disableSet;
	@FXML
	private TextField nameField;
	@FXML
	private TextField dateOfBirthField;
	@FXML
	private Button setButton;

	@FXML
	private void initialize() {
		formatter.setLenient(false);
		setButton.setTooltip(new Tooltip("Set new day of birth for name"));
		name = nameField.textProperty();
		dateOfBirthField.textProperty()
				.bindBidirectional(date, new DateConverter());
		disableSet = setButton.disableProperty();
		name.addListener((prop, oldVal, newVal) -> date.set(database
				.readDateOfBirth(newVal)));
		date.addListener((prop, oldVal, newVal) -> disableSet.set(newVal == null
				|| name.get().length() == 0));
		disableSet.set(true);
	}

	@FXML
	private void setDate() {
		if (date.get() != null && name.get().length() != 0) database
				.storeDateOfBirth(name.get(), date.get());
	}

	private class DateConverter extends StringConverter<Date> {
		@Override
		public String toString(Date date) {
			return date != null ? formatter.format(date) : "";
		}

		@Override
		public Date fromString(String string) {
			Date date = null;
			try {
				date = formatter.parse(string);
			}
			catch (ParseException e) {}
			return date;
		}
	}
}