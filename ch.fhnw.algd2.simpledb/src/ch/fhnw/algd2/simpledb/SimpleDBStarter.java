/*
 * Created on 08.05.2015
 */
package ch.fhnw.algd2.simpledb;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Wolfgang Weck
 */
public class SimpleDBStarter extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("gui/SimpleDBGUI.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Simple DB");
		stage.show();
	}
}