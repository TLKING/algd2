/*
 * Created on 08.05.2015
 */
package ch.fhnw.algd2.simpledb.database;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Wolfgang Weck
 */
public class SimpleDB {
	private Map<String, Date> map = new HashMap<>();

	public void storeDateOfBirth(String name, Date date) {
		System.out.println("set " + name + " " + date.toString());
		map.put(name, date);
	}

	public Date readDateOfBirth(String name) {
		System.out.println("get " + name);
		return map.get(name);
	}
}
