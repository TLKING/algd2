package ch.fhnw.algd2.collections.list.linkedlist;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import ch.fhnw.algd2.collections.list.MyAbstractList;

public class DoublyLinkedList<E> extends MyAbstractList<E> {
	private int size = 0;
	private Node<E> first, last;

	@Override
	public boolean add(E e) {
		if (first == null) { // handle first entry differently
			first = new Node<E>(e);
			last = first;
		} else {
			last.next = new Node<E>(last, e, null);
			last = last.next;
		}

		size++;
		modCount++;

		return true;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index > size)
			throw new IndexOutOfBoundsException();

		if (first == null && index == 0) {
			first = new Node<E>(element);
			last = first;
		} else {
			if (index == 0) {
				first = new Node<E>(null, element, first);
				first.next.prev = first;
			} else if (index == size) {
				last = new Node<E>(last, element, null);
				last.prev.next = last;
			} else {
				Node<E> prev = null;
				Node<E> node = first;

				while (index > 0) {
					prev = node;
					node = node.next;
					index--;
				}

				prev.next = new Node<E>(prev, element, node);
				node.prev = prev.next;
			}
		}

		modCount++;
		size++;
	}

	@Override
	public boolean remove(Object o) {
		if (first == null || !contains(o))
			return false;

		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			node = node.next;
		}

		if (node.prev == null && node.next == null) {
			first = null;
			last = null;
		} else {
			if (node.prev == null) {
				first = node.next;
				first.prev = null;
			} else if (node.next == null) {
				last = node.prev;
				last.next = null;
			} else {
				node.prev.next = node.next;
				node.next.prev = node.prev;
			}
		}

		modCount++;
		size--;

		return true;
	}

	@Override
	public E remove(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		while (index > 0) {
			node = node.next;
			index--;
		}

		if (node.prev == null && node.next == null) {
			first = null;
			last = null;
		} else {
			if (node.prev == null) {
				first = node.next;
				first.prev = null;
			} else if (node.next == null) {
				last = node.prev;
				last.next = null;
			} else {
				node.prev.next = node.next;
				node.next.prev = node.prev;
			}
		}

		modCount++;
		size--;

		return node.elem;
	}

	@Override
	public boolean contains(Object o) {
		if (first == null)
			return false;

		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			node = node.next;
		}
		return o == node.elem || (o != null && o.equals(node.elem));
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		while (index > 0) {
			node = node.next;
			index--;
		}

		return node.elem;
	}

	@Override
	public Object[] toArray() {
		return arrayForDoublyLinkedList();
		// return arrayForCyclicDoublyLinkedList();
	}

	private Object[] arrayForDoublyLinkedList() {
		Object[] array = new Object[size];
		int index = 0;
		Node<E> current = first;
		while (current != null) {
			array[index++] = current.elem;
			current = current.next;
		}
		return array;
	}

	// private Object[] arrayForCyclicDoublyLinkedList() {
	// Object[] array = new Object[size];
	// int index = 0;
	// Node<E> current = first.next;
	// while (current != first) {
	// array[index++] = current.elem;
	// current = current.next;
	// }
	// return array;
	// }

	@Override
	public int size() {
		return size;
	}

	@Override
	public Iterator<E> iterator() {
		return new MyListIterator();
	}

	@Override
	public ListIterator<E> listIterator() {
		throw new UnsupportedOperationException("Implement later");
	}

	private static class Node<E> {
		private E elem;
		private Node<E> prev, next;

		private Node(E elem) {
			this.elem = elem;
		}

		private Node(Node<E> prev, E elem, Node<E> next) {
			this.prev = prev;
			this.elem = elem;
			this.next = next;
		}
	}

	private class MyListIterator implements Iterator<E> {
		private Node<E> prev = null;
		private Node<E> node = null;
		private Node<E> next = first;
		private int itModCount = modCount;
		private boolean mayRemove = false;

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public E next() {
			if (!hasNext())
				throw new NoSuchElementException();
			if (itModCount != modCount)
				throw new ConcurrentModificationException();

			prev = node;
			node = next;
			next = next.next;
			mayRemove = true;
			return node.elem;
		}

		@Override
		public void remove() {
			if (!mayRemove)
				throw new IllegalStateException();
			if (itModCount != modCount)
				throw new ConcurrentModificationException();

			if (prev == null) {
				first = next;
			} else if (node.next == null) {
				last = prev;
				last.next = null;
			} else {
				prev.next = node.next;
				node = prev;
			}

			mayRemove = false;
			modCount++;
			itModCount++;
			size--;
		}
	}

	public static void main(String[] args) {
		DoublyLinkedList<Integer> list = new DoublyLinkedList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		System.out.println(Arrays.toString(list.toArray()));
	}
}
