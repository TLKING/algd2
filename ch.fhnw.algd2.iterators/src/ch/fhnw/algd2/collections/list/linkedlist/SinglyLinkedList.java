package ch.fhnw.algd2.collections.list.linkedlist;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import ch.fhnw.algd2.collections.list.MyAbstractList;

public class SinglyLinkedList<E> extends MyAbstractList<E> {
	// variable int modCount already declared by AbstractList<E>
	private int size = 0;
	private Node<E> first;

	@Override
	public boolean add(E e) {
		if (first == null) { // handle first entry differently
			first = new Node<E>(e);
		} else {
			Node<E> node = first;

			while (node.next != null) {
				node = node.next;
			}

			node.next = new Node<E>(e);
		}
		size++;
		modCount++;
		
		return true;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index > size)
			throw new IndexOutOfBoundsException();

		if (index == 0) {
			first = new Node<E>(element, first);
		} else {
			Node<E> prev = null;
			Node<E> node = first;

			while (index > 0) {
				prev = node;
				node = node.next;
				index--;
			}

			prev.next = new Node<E>(element, node);
		}

		size++;
		modCount++;
	}

	@Override
	public boolean remove(Object o) {
		if (first == null || !contains(o))
			return false;

		Node<E> prev = null;
		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			prev = node;
			node = node.next;
		}

		if (prev == null) {
			first = node.next;
		} else {
			prev.next = node.next;
		}

		size--;
		modCount++;

		return true;
	}

	@Override
	public E remove(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		if (index == 0) {
			first = first.next;
		} else {
			Node<E> prev = null;

			while (index > 0) {
				prev = node;
				node = node.next;
				index--;
			}

			if (prev == null) {
				first = node.next;
			} else {
				prev.next = node.next;
			}
		}

		size--;
		modCount++;

		return node.elem;
	}

	@Override
	public boolean contains(Object o) {
		if (first == null)
			return false;

		Node<E> node = first;

		while (node.next != null && !(o == node.elem || (o != null && o.equals(node.elem)))) {
			node = node.next;
		}
		return o == node.elem || (o != null && o.equals(node.elem));
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException();

		Node<E> node = first;

		while (index > 0) {
			node = node.next;
			index--;
		}

		return node.elem;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		int index = 0;
		Node<E> current = first;
		while (current != null) {
			array[index++] = current.elem;
			current = current.next;
		}
		return array;
	}

	@Override
	public int size() {
		return size;
	}

	private static class Node<E> {
		private final E elem;
		private Node<E> next = null;

		private Node(E elem) {
			this.elem = elem;
		}

		private Node(E elem, Node<E> next) {
			this.elem = elem;
			this.next = next;
		}
	}

	@Override
	public Iterator<E> iterator() {
		return new MyIterator();
	}

	private class MyIterator implements Iterator<E> {
		private Node<E> prev = null;
		private Node<E> node = null;
		private Node<E> next = first;
		private int itModCount = modCount;
		private boolean mayRemove = false;

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public E next() {
			if (!hasNext())
				throw new NoSuchElementException();
			if(itModCount != modCount)
				throw new ConcurrentModificationException();
			
			prev = node;
			node = next;
			next = next.next;
			mayRemove = true;
			return node.elem;
		}

		@Override
		public void remove() {
			if(!mayRemove)
				throw new IllegalStateException();
			if (itModCount != modCount)
				throw new ConcurrentModificationException();
			
			if(prev == null) {
				first = next;
			} else {
				prev.next = node.next;
				node = prev;
			}
			
			mayRemove = false;
			modCount++;
			itModCount++;
			size--;
		}
	}

	public static void main(String[] args) {
		SinglyLinkedList<Integer> list = new SinglyLinkedList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		System.out.println(Arrays.toString(list.toArray()));
	}
}
